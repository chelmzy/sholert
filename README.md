# sholert

Sholert is a collection of scripts that monitor changes on a specific network using the Shodan API

There are two different methods of monitoring a network:

alert.py - Checks only for port changes by getting current API results and diffing against current known. This script also writes the results to syslog.

stream.py - Utilizes the built in Shodan firehose. This script alerts to any and all changes. This includes HTTP Date headers, HTML changes, etc.

I recommend setting up stream.py as a service and using a cronjob for alert.py