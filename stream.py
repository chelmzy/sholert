#!/usr/bin/python
from shodan import Shodan
from shodan.helpers import get_ip
from shodan.cli.helpers import get_api_key


# Configuration
EMAIL_TO = 'user@example.com'
EMAIL_FROM  = 'firewall@example.com'
SMTP_SERVER = 'Internal SMTP Relay'

def send_mail(subject, content):
    """Send an email using a local mail server."""
    from smtplib import SMTP
    server = SMTP()
    server.connect(SMTP_SERVER)
    server.sendmail(EMAIL_FROM, EMAIL_TO, 'Subject: {}\n\n{}'.format(subject, content))
    server.quit()

# Setup the Shodan API connection
api = Shodan('apikeyhere')

# Subscribe to results for all networks:
for banner in api.stream.alert():
        ip = get_ip(banner)
        send_mail('Firewall Change: {}'.format(ip), """
Firewall Change Information:

Port: {}
Data: {}

""".format(banner['port'], banner['data']))