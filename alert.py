#!/usr/bin/python3
from shodan import Shodan
from shodan.helpers import get_ip
from shodan.cli.helpers import get_api_key
from time import sleep
import logging
import logging.handlers

# Create logger
logger = logging.getLogger('myLogger')
logger.setLevel(logging.INFO)

# Add handler to the logger
handler = logging.handlers.SysLogHandler('/dev/log')

# Add formatter to the handler
formatter = logging.Formatter('<Shodan.IO Open Ports> %(message)s')
handler.formatter = formatter
logger.addHandler(handler)

# Configuration
EMAIL_TO = 'user@example.com'
EMAIL_FROM  = 'firewall@example.com'
SMTP_SERVER = 'relay.example.com'

def send_mail(subject, content):
    """Send an email using a local mail server."""
    from smtplib import SMTP
    server = SMTP()
    server.connect(SMTP_SERVER)
    server.sendmail(EMAIL_FROM, EMAIL_TO, 'Subject: {}\n\n{}'.format(subject, content))
    server.quit()

def Diff(li1, li2): 
    return (list(set(li1) - set(li2))) 

# Setup the Shodan API connection
api = Shodan('')

# Set the Shodan query
query = ''

# Run the Shodan query
result = api.search_cursor(query)

# Make sure results file is present
filename = 'shoresults.txt'
try:
    file = open(filename, '+r')
except FileNotFoundError:
    file = open(filename, 'w')
    file = open(filename, '+r')

# Set list variables    
newlist = []
currentlist = file.readlines()

# Iterate through the results. Requests limited to 1/s. Do I really have to iterate through each service on each host or is there an easier way to return the full list of services with a query wtf?
for host in result:
    host = api.host(host['ip_str'])
    for item in host['data']:

        ip = item['ip_str']
        port = str(item['port'])
        output = ip + ':' + port + "\n"
        newlist.append(output)
    sleep(1)

# Get list difference
isDiff = Diff(newlist,currentlist)

# Write newlist to syslog
for item in newlist:
        logger.info(str(item))

# Check if there is a difference. If so then an email alert is sent and the new values are written to file
if isDiff != []:
    send_mail('Firewall Change', "Firewall Change Information:\n" + "".join(isDiff))
    with open('shoresults.txt', 'w') as f:
        f.write("".join(newlist))
    
